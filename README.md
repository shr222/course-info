# CSE 109 - Systems Software - Summer 2021

- Dates: 7/6/2021 - 8/12/2021
- Meeting Times: Remote Asynchronous

## Instructor

### [Prof. Corey Montella](https://engineering.lehigh.edu/cse/faculty/5300)
- E-mail address: [cim310@lehigh.edu](mailto:cim310@lehigh.edu)
- Gitlab: https://gitlab.com/profmontella
- Office hours: By appointment

## Course Description
   
Advanced programming and data structures, including dynamic structures, memory allocation, data organization, symbol tables, hash tables, data files. Practical methods for implementing medium-scale programs. Basic networking concepts.

## Course Learning Objectives

- Introduction to Git, Bash, UNIX, and emacs
- C and C++
- Review of structures shared by Java and C/C++
- Passing variable to functions
- Pointers
- RAII
- Makefiles
- Hashing, Data Structures
- Text file reading and writing
- Binary file reading and writing
- Classes and Subclasses
- Templated Classes
- Debugging using gdb
- Parsing and compilation
- Assembler Basics
- Linker Basics
- Loader Basics
- Building Mid-Size Programs
- Data serialization
- Networking

## Prerequisites

CSE 017 or CSE 018.

## Online Classroom

This year, CSE 109 is a global class. We have students around the world, which means the course needs to operate in a way that accommodates a variety of schedules and time zones. To this end, we will use a number of online tools to help administer this course in an online-first way. There will be no required in-person meetings. All lectures and recitations will be recorded and available on YouTube at the link in the list below. We will conduct most of our communication via Slack, which will help keep us connected as a class. 

Each week we will have 4 classes. On Monday, Tuesday, and Wednesday two recorded lectures will be released each day. On Thursday, we will have a live recitation at an agreeable time at the Zoom link in the list below. Recitations will be recorded and uploaded to a Lehigh Google Drive folder, where you can access them at any time. You will also have the opportunity to submit questions for me to answer during this recitation if you can't attend live and you want something answered.

- [Repository](https://gitlab.com/lehigh-cse-109/summer-2021/course-info) - this will serve as our course website for the semester. All course resources will be centralized here.
- [Assignments](https://gitlab.com/lehigh-cse-109/summer-2021/assignments) - this private repository is where assignments and exams will be posted. You will need to give your Gitlab ID to the instructor once you've signed up. He will grant you access to the repository, and you will be free to view and fork assignments.
- [Zoom Class Link](https://lehigh.zoom.us/j/5245765302) - Used for recitations and instructor office hours.
- [Lectures](https://youtube.com/playlist?list=PL4A2v89SXU3TS1fcYFmXi-tch8p0LucE7) - A playlist of lectures ([Drive](https://drive.google.com/drive/folders/1SJgWVeEnhB9ql4i_MXrJL-H2D-OXvHbs?usp=sharing)).
- [Slack](https://join.slack.com/t/lu-cse109-su21/shared_invite/zt-skovnww0-Vb3sTpbegNdSKBmyJ_1SEA) - We will use slack for synchronous communication. I will post updates on new assignments here, as well as class announcements. I will typically send you a message on slack if I want to get a hold of your, rather than your e-mail.
- [Piazza](https://piazza.com/lehigh/summer2021/cse109) - You can ask and answer questions about course material on Piazza. There is a feature for anonymous posting (the instructor can still see who you are). Using piazza to ask, answer, or even view questions counts toward your participation grade.
- [Coursesite](https://coursite.lehigh.edu) - Coursesite will be used exclusively to distribute grades in a secure manner. There will be no course content posted on Coursesite.

## Required Texts

Think C/C++
by Allen B. Downey

Along with a variety of online resources, we will use this textbook as a reference for the C and C++ languages:
https://greenteapress.com/wp/think-c/

## Evaluation Criteria

- Homework - 40%
- Quizzes - 20%
- Midterm - 20%
- Final - 20% 

Grades will be assigned on a letter scale with the following percentage threshods:

```
A  [93, 100]
A- [90, 93)
B+ [87, 90)
B  [83, 87)
B- [80, 83)
...
F [0, 60)
```

### Homework

Homework will be assigned on a generally weekly basis. It may take the form of programming assignments, written assignments, or solving problem sets. Some assignments will be automatically graded against a set of tests, others will be graded by hand by a team of student graders. Assignments will be posted on Gitlab and will be due by the indicated date by the end of the day Lehigh Time.

### Quizzes

Weekly quizzes will be available by the end of day every Thursday. They will be due by the end of the day on the date indicated on the quiz. Quiz content will test your understanding of the week's lecture topics, and will be completely open book, and open internet.

### Exams

We will have two exams this semester: one midterm, and one comprehensive final. More info will follow closer to the exam dates.

## Course Policies

### Covid19 Statement

Due to the ongoing pandemic, this course will be held entirely online. There are no in person meetings necessary. We will hold a live recitation each week via Zoom. During this meeting, you can choose whether or not to turn on your camera. If you choose to turn on your camera, please make sure you are wearing something that would be appropriate to wear to a live classroom setting. As the pandemic is a fluid situation, we reserve the right to make updates to this syllabus as the pandemic evolves.

### Time Zones

Unless otherwise specified, all deadlines and meeting times are communicated relative to Lehigh University, which sits in the Eastern time zone. At the beginning of the course the time shift is GMT-4, but on Sunday, November 1 our clocks will be adjusted back one hour for the remaining of the semester. This may or may not coincide with a similar shift in time for your local region, so please be cognizant of this possibility when submitting assignments.

### Version Control

We will use a version control system (VCS) called "git" to turn in all assignments and exams in this course. This website, Gitlab, is an online interface for the git VCS, but it is also available as a command line utility and a desktop utility. You can do the work in this course in any of these contexts.

Git is useful for a couple reasons. First, it is a practical tool that is used commonly in the software development industry. It is useful for coordinating the work of a team of developers that may be distributed across the globe. This means you choose a career as a software developer, you will likely use a tool like git as part of your job, so it's great practice to get used to using it here. But more importantly, in the context of a classroom, git allows the instructor to monitor the progress of your work to detect anomalies like cheating. 

### Late Assignments

Late assignments are accepted without explanation for the first occurance. They will incur a penalty of one letter grade (10%) per day late, up to two late days. After two days late the assignment will not be accepted (as the solution will have been posted by then). After the first late assignment, an explanation for the late assignment will be required for future late assignments. The instructor reserves the right to not accept assignments provided without sufficient explanation.

### Exam Makeup

All students are expected to complete the midterm and finals exams during their scheduled windows. These windows are to give you flexibility in deciding when you will complete your exam. If you have an excused conflict with either the midterm or final exam windows, we can accommodate your schedule. Excused conflicts are those activities sanctioned by the University, or any medical condition that necessitates an exception is warranted.
Please make arrangements with the instructor for an alternative testing situation. Inform the instructor as soon as you know a conflict exists.

### Office Hours

In general, I will only hold office hours by appointment. If you want one-on-one help, I will be more than happy to meet with you over Zoom, or you can join a zoom chat with one of our many graders. However, **no 1:1 in-person office hours will be granted by the instructor within 48 hours of an assignment or exam deadline**. This is to encourage you to start your assignments and studying early. There will be grader office hours available for help.

### Statement on Academic Integrity

The programs you write in this course should be original and written for this course specifically. You can't submit solutions found on the internet, and you can't use projects you've written for other courses. At times we will use third party libraries or example code as a basis for your own code. This is permissible if the included code is cited.

My view is that students resort to cheating when they feel desperate. If you're running out of time and you need an extension, ask for one rather than cheat. If you are completely lost and feel like you don't understand the assignment at all, talk to a grader or the instructor and they will help you get started. If you're worried about getting a poor grade on a project or an exam, consider a poor grade will be forgotten soon after you graduate, while getting caught cheating will long be remembered. Just submit your own work and you'll probably get partial credit.

## University Policies

### The Principles of Our Equitable Community

The Principles of Our Equitable Community: Lehigh University endorses The Principles of Our Equitable Community (www.lehigh.edu/diversity). We expect each member of this class to acknowledge and practice these Principles. Respect for each other and for differing viewpoints is a vital component of the learning environment inside and outside the classroom.

### Accommodations for Students with Disabilities

Lehigh University is committed to maintaining an equitable and inclusive community and welcomes students with disabilities into all of the University's educational programs. In order to receive consideration for reasonable accommodations, a student with a disability must contact
Disability Support Services (DSS), provide documentation, and participate in an interactive review process. If the documentation supports a request for reasonable accommodations, DSS will provide students with a Letter of Accommodations. Students who are approved for accommodations at Lehigh should share this letter and discuss their accommodations and learning needs with instructors as early in the semester as possible. For more information or to request services, please contact Disability Support Services in person in Williams Hall, Suite 301, via phone at 610-758-4152, via email at indss@lehigh.edu, or online at [https://studentaffairs.lehigh.edu/disabilities](https://studentaffairs.lehigh.edu/disabilities).

### Lehigh University Policy on Harassment and Non-Discrimination

Lehigh University upholds The Principles of Our Equitable Community and is committed to providing an educational, working, co-curricular, social, and living environment for all students, staff, faculty, trustees, contract workers, and visitors that is free from harassment and
discrimination on the basis of age, color, disability, gender identity or expression, genetic information, marital or familial status, national or ethnic origin, race, religion, sex, sexual orientation, or veteran status. Such harassment or discrimination is unacceptable behavior and will not be tolerated. The University strongly encourages (and, depending upon the circumstances, may require) students, faculty, staff or visitors who experience or witness harassment or discrimination, or have information about harassment or discrimination in University programs or activities, to immediately report such conduct.
